PYEARTH
====================

Application allowing to compute the distance between two cities from a database and show their position on the globe. 

INSTALLATION
-------------------

Install python3 and pyqt4. For example, on debian-like systems:

    apt-get install python3 python3-pyqt4


To run the application, go to py-earth directory and run

    python3 pyearth

That's all!


USAGE
-------------------

1. Choose the starting point
2. Choose the running mode:
    - Prudent: you'll have to select the end point as well.
    - Random
    - Aventure
3. Read the chosen ending point and the distance from the starting point on the bottom of the control pannel. 
On the right panel, the green flag indicates the starting point position on the Earth and the red one shows the end point. You can move the Earth left/right with the arrows located under the image.


DEVELOPPERS
-------------------

The graphical interface is created with Qt4 via PyQt. 

Package is organized as follows:

- src
    - models: data representations and relation to database(s)
    - views: the GUI with Qt4
- images
- styles: *.qss files
- translations: *.qm and *.ts files
- data: databases

