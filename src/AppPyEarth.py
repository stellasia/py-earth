#!/usr/bin/python
# -*- coding: utf-8 -*-

""" 
"""
#author: E. Scifo
#date: 28/12/13
#licence: 


######################################################################
import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import * 

from views.MainWindow import *


######################################################################
class AppPyEarth(QApplication):
    """ Main application """

    ##================================
    def __init__(self, n):
        """ """ 
        super(AppPyEarth, self).__init__(n) # The super() method returns the parent object of the Example class and we call its constructor
        self.chargeSettings()
        ## Change the styke
        if self.style=="plastique":
            self.setStyle(self.style) 
        else:
            fil = QFile(os.path.join(os.path.dirname(__file__), "styles/", self.style+".css"))
            fil.open(QFile.ReadOnly)
            styleSheet = fil.readAll().data()
            self.setStyleSheet(styleSheet.decode("utf-8"))

        ## Translation process
        locale = self.language#.toString()
        #locale = QLocale.system().name()
        # qtTranslator = QTranslator()
        # if qtTranslator.load("qt_" + locale):
        #     self.installTranslator(qtTranslator)
        appTranslator = QTranslator()
        if appTranslator.load(os.path.join(os.path.dirname(__file__), "translations/pyearth_"+ locale)):
            self.installTranslator(appTranslator)

        ## Instanciate MainWindow
        mw = MainWindow()
        mw.style = self.style
        mw.language = self.language
        mw.show()
        ## Resize the window        
        screen = QApplication.desktop().screenGeometry()
        mw.move( screen.center() - mw.rect().center() )
        
        ## launch the application
        self.exec_()

        ## when the main window is closed, get the values of the options
        self.style = mw.style
        self.language = mw.language
        # exit method allows to save the settings
        self.exit()


    ##================================
    def chargeSettings(self):
        """ Get back the user personnal settings from a file saved on the user system (QSettings) (the location depends on the OS).  """ 
        settings = QSettings("devience", "PyEarth");
        self.style = settings.value("User/style", "plastique")#.toString()
        self.language = settings.value("User/language", QLocale.system().name())#.toString()
        return None


    ##================================
    def exit(self):
        """ Overriden closeEvent method to save the settings before exiting. """
        ## Settings:
        settings = QSettings("devience", "PyEarth");
        # Window size and position
        settings.setValue("WinSizePos/test", True);
        # Language and stylesheet
        settings.setValue("User/language", self.language);
        settings.setValue("User/style", self.style);
#        e.accept() # accept the event in any case, ie the application will be closed
