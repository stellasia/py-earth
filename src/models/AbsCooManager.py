#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Generic class for coordinate 
database managers. """


######################################################################
from math import cos, sin, acos, pi


######################################################################
RAYON_TERRE = 6371 #km


######################################################################
class AbsCooManager:
    """ Abstract class for coordinate database management. """

    ##================================
    def __init__(self, p_datafilename):
        """ Initializer """
        self.datafilename = p_datafilename

    ##================================
    def getCityList(self):
        """ Get all the cities in the database. """
        return None

    ##================================
    def getCoo(self, p_city):
        """ Get the coordinates of a given city. """
        return None

    ##================================
    def getDistance(self, p_city_start, p_city_end):
        """ Compute distance between two cities (assuming R=1). """
        coo1 = self.getCoo(p_city_start)
        coo2 = self.getCoo(p_city_end)
        delta_L = coo1.latitude_rad - coo2.latitude_rad
        dist = acos( sin(coo1.longitude_rad) * sin(coo2.longitude_rad) + cos(coo1.longitude_rad) * cos(coo2.longitude_rad) * cos(delta_L))
        return dist*RAYON_TERRE

    ##================================
    def getFarestCity(self, p_city_start):
        """ Compute distance between a given city 
        and all the other ones and find the farest 
        city in the database. """
        distance_max = -1
        city_end = ""
        allCities = self.getCityList()
        for city in allCities:
            dist = self.getDistance(p_city_start, city)
            if dist > distance_max:
                distance_max = dist
                city_end = city
        return city_end, distance_max




######################################################################
if __name__=='__main__':
    # this is an abstract class so can not be tested... 
    pass
