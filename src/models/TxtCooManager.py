#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Class for database as a text format:
"city lat long"
"""

######################################################################
import os
from math import cos, sin, acos

from models.AbsCooManager import *
from models.Coordinate import *


######################################################################
RAYON_TERRE = 6371 #km


######################################################################
class TxtCooManager(AbsCooManager) :
    """ Class for coordinate text database management. """

    ##================================
    def __init__(self, p_datafilename="data_small.txt"):
        fullFileName = os.path.join(os.path.dirname(__file__), '../../data/', p_datafilename)
        AbsCooManager.__init__(self, fullFileName)
        self._file = open(fullFileName, "r") # only in read mode, does not allow to modify the file for the time being

    ##================================
    def getCityList(self, p_firstLetters=""):
        """ Get all the cities in the database. """
        cities = []
        for line in self._file:
            liste = line.split()
            if len(liste)!=3:
                continue # if the line does not contain the 3 expected elements, ignore it
            city = liste[0]
            if p_firstLetters=="" or city[0:len(p_fistLetters)] == p_firstLetters:
                cities.append(city)
        self._file.seek(0) # go back to the beginning of the file for the next search (algo can be improved there)
        return cities

    ##================================
    def getCoo(self, p_city):
        """ Get the coordinates of a given city (angles in degrees). """
        coo = Coordinates()
        coo.setCity(p_city)
        for line in self._file:
            liste = line.split()
            if len(liste)!=3:
                continue
            if liste[0] == p_city:
                coo.setLatitude(float(liste[1]))
                coo.setLongitude(float(liste[2]))
                break
        self._file.seek(0) # go back to the begining of the file... 
        return coo

 
#####################################################################
if __name__=='__main__':
    txtCooMan = TxtCooManager("data_small.txt")
    print("\n".join(txtCooMan.getCityList()))
    print(txtCooMan.getCoo("Paris"))

