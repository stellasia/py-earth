#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Class to handle coordinates using the following format:
[city_name, latitude, longitude]
Can be extended if needed.
 """


######################################################################
from math import cos, sin, pi


######################################################################
class Coordinates:
    """ Class to handle the coorfinates of a city on the Globe (latitude, longitude) and on the map (x,y)."""

    ##================================
    def __init__(self, liste=[]):
        """ liste format : [str(city), float(lat), float(long)] """
        if len(liste) == 3:
            self.setCity(liste[0])
            self.setLatitude(liste[1])
            self.setLongitude(liste[2])
        self._x = 0
        self._y = 0 
    
    ##================================
    def __str__(self):
        return self.city + " has latitude " + str(self.latitude) + " deg. and longitude " + str( self.longitude) + " deg."

    ##================================
    def _convert(self, p_angle_degre):
        return pi*p_angle_degre/180

    ##================================
    def setLatitude(self, p_latitude):
        if isinstance(p_latitude, float) or isinstance(p_latitude, int):
            self.latitude = p_latitude
        else:
            raise ValueError("Coo.setLatitude==> Latitude should be a number (int or float), ", type(p_latitude), "given.")
        return None
    ##================================
    def setLongitude(self, p_longitude):
        if isinstance(p_longitude, float) or isinstance(p_longitude, int):
            self.longitude = p_longitude
        else:
            raise ValueError("Coo.setLongitude==> Longitude should be a number (int or float)", type(p_longitude), "given.")
        return None
    ##================================
    def setCity(self, p_city):
        if isinstance(p_city, str) or isinstance(p_city, unicode):
            self.city = p_city
        else:
            raise ValueError("Coo.setCity==> City must be a string !", type(p_city), "given.")
        return None

    ##================================
    @property
    def latitude_rad(self):
        return self._convert(self.latitude)

        ##================================
    @property
    def longitude_rad(self):
        return self._convert(self.longitude)

    
    ##================================
    def getXYZ(self, p_long_ref=0):
        """ Suppose an 'earth' radius of 1 """
        self._x = sin(self.latitude_rad)*sin(self.longitude_rad-p_long_ref)
        self._y = cos(self.latitude_rad)
        self._z = sin(self.latitude_rad)*cos(self.longitude_rad-p_long_ref)
        return self._x, self._y, self._z
    


######################################################################
if __name__=='__main__':
    mycoo = Coordinates(["Zorglub", 1.0, 90])
    print(mycoo.getLatitudeRad())
    print(mycoo.getXYZ(0))


    mycoo2 = Coordinates()
    mycoo2.setCity("Orsay")
    print(mycoo2.getCity())
#    mycoo2.setLatitude("djsof") # test the exception raising
