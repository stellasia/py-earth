#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Class for database as a sql format
"""

######################################################################
import os
from math import cos, sin, acos

from models.AbsCooManager import *
from models.Coordinate import *


######################################################################
class SqlCooManager(AbsCooManager) :
    """ Class for coordinate text database management. """

    ##================================
    def __init__(self, p_datafilename="data_medium.sql"):
        fullFileName = os.path.join(os.path.dirname(__file__), '../../data/', p_datafilename)
        #print ("file name", fullFileName)
        AbsCooManager.__init__(self, fullFileName)


    ##================================
    def getCityList(self, p_firstLetters=""):
        """ Get all the cities in the database. """
        cities = []
        return cities


    ##================================
    def getCoo(self, p_city):
        """ Get the coordinates of a given city (angles in degrees). """
        return 

 
#####################################################################
if __name__=='__main__':
    pass
