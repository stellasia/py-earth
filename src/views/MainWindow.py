#!/usr/bin/python
# -*- coding: utf-8 -*-

""" 
MainWindow containing a 
ControlPannel and an 
ImageDisplay widget. 
Also defines the menu.
"""
#author: E. Scifo
#date: 28/12/13
#licence: 


######################################################################
import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import * 

from views.ControlPannel import *
from views.ImageDisplay import *
from views.OptionBox import *
from models.TxtCooManager import *

about_message = """ 
<h1> About and licence </h1>
<p>This application was developped by <b>E. Scifo</b> using <a href="http://www.python.org/">Python</a> and <a href="http://qt.digia.com">Qt</a> through the <a href="http://www.riverbankcomputing.co.uk/software/pyqt/intro">PyQt</a> framework. It is distributed under a free licence.</p>

<h1> Help </h1>
<p>Its aim is to compute the distance between two cities on the Globe, using simple mathematics formulae (see for more information about the mathematics). It  is associated with a graphical user interface allowing to choose a starting and end city among a database. </p>

<p>
Three modes are allowed:
<ul>
<li>Prudent: choose the start and end point and the program will just compute the distance between both;</li>
<li>Joueur: you choose a starting point and the program chooses randomly an end point from the database;</li>
<li>Aventurier: you choose the starting point and the program finds the farest city among the ones in the database.</li>
</ul>
</p>

<p> Have fun using <b>PyEarth</b> and visit <a href="">devience.fr</a> in case of more questions.</p>
"""


######################################################################
class MainWindow(QMainWindow):
    """ Controller of the application """

    ##================================
    def __init__(self):
        """ """ 
        super(MainWindow, self).__init__() # The super() method returns the parent object of the Example class and we call its constructor
        
        self._cooManager = TxtCooManager() # coo manager
        self._cities = self._cooManager.getCityList()
        self.language = None
        self.style = None
        #        self.chargeSettings() # set the user preferences
        self.initUI() # init the user interface (control pannal and image)


    ##================================
    def initUI(self):
        """ Initialize the user interface. """ 

        self.setWindowTitle('PyEarth')    
        self.setWindowIcon(QIcon(os.path.join(os.path.dirname(__file__), '../../', 'images/globe-Vista-icon.png')))     
        self.statusBar().showMessage(self.tr('Ready'))

        self._controlPannel = ControlPannel(self._cities)        
        self._image = ImageDisplay() 

        self.connect(self._controlPannel, SIGNAL("updateControlPannelSignal()"),
             self.updateImage) # if the control pannel is updated, see the updateImage method


        main_layout = QHBoxLayout()
        main_layout.addWidget(self._controlPannel)
        main_layout.addWidget(self._image)

        zoneCentrale = QWidget()
        zoneCentrale.setLayout(main_layout)
        self.setCentralWidget(zoneCentrale)

        self.initMenu() # init the menubar

        self.updateImage() # first image update with the default settings


    ##================================
    def initMenu(self):
        """ Initialize the main menu bar:
        - File: quit
        - Tools: change DB and options (the latter with OptionBox class)
        - Help: show the about window
        """
        a_exit = QAction(QIcon(QIcon(os.path.join(os.path.dirname(__file__), '../../', 'images/exit.png'))), self.tr('&Exit'), self)        
        a_exit.setShortcut('Ctrl+Q')
        a_exit.setStatusTip(self.tr('Exit application'))
        a_exit.triggered.connect(qApp.quit)

        a_useFrenchDB = QAction(QIcon(os.path.join(os.path.dirname(__file__), '../../', 'images/change.png')), self.tr('Use French (&Default) DB'), self)        
        a_useFrenchDB.setShortcut('Ctrl+D')
        a_useFrenchDB.setStatusTip(self.tr('French DB'))
        a_useFrenchDB.triggered.connect(self.changeDB)

        a_useWorldDB = QAction(QIcon(os.path.join(os.path.dirname(__file__), '../../', 'images/change.png')), self.tr('Use &Wolrd DB'), self)        
        a_useWorldDB.setShortcut('Ctrl+W')
        a_useWorldDB.setStatusTip(self.tr('World DB'))
        a_useWorldDB.triggered.connect(self.changeDB)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu(self.tr('&File'))

        toolMenu = menubar.addMenu(self.tr('&Tools'))
        changeDBMenu = toolMenu.addMenu(self.tr("&Change database"))
        changeDBMenu.addAction(a_useFrenchDB)
        changeDBMenu.addAction(a_useWorldDB)

        a_options = QAction(QIcon(os.path.join(os.path.dirname(__file__), '../../', 'images/options.png')), self.tr('&Options'), self)        
        a_options.setShortcut('Ctrl+L')
        a_options.setStatusTip(self.tr('Options'))
        a_options.triggered.connect(self.changeOptions)
        toolMenu.addAction(a_options)       

        helpMenu = menubar.addMenu(self.tr('&Help'))
        a_about = QAction(QIcon(os.path.join(os.path.dirname(__file__), '../../', 'images/about.png')), self.tr('&About'), self)        
        a_about.setShortcut('Ctrl+A')
        a_about.setStatusTip(self.tr('About'))
        a_about.triggered.connect(self.about)
        helpMenu.addAction(a_about)       

        fileMenu.addSeparator()
        fileMenu.addAction(a_exit)

        return None


    ##================================
    def changeDB(self):
        """ Slot to change the DB file and method (txt, kml or sql) """
        reply = QMessageBox.question(self, 'Message',
                                     "Change database method", QMessageBox.Ok)
        return None


    ##================================
    def changeOptions(self):
        """ Slot to change the options of the application. """
        dialog = OptionBox()    
        if  dialog.exec_()==QDialog.Accepted:
            if dialog.isModified:
                reply = QMessageBox.warning(dialog, self.tr('Warning'),
                                            self.tr("The changes will only be taken into account once the program is restarted."), QMessageBox.Ok)

                self.language, self.style = dialog.getValues()        
        return None


    ##================================
    def about(self):
        """ Slot called when menu About is pressed. Just print a message in a box. """
        reply = QMessageBox.question(self, 'About',
                                     self.tr(about_message), QMessageBox.Ok)
        return None


    ##================================
    def GetControlPannel(self):        
        return self._controlPannel


    ##================================
    def updateImage(self):
        """ Send update signal to the view when the parameters are changed in the control pannel. """
        coo_start=self._cooManager.getCoo(self._controlPannel.GetDeparture())
        coo_end=self._cooManager.getCoo(self._controlPannel.GetEnd())
        self._image.update(coo_start, coo_end)
        distance = self._cooManager.getDistance(self._controlPannel.GetDeparture(), self._controlPannel.GetEnd())
        self._controlPannel.SetDistance(distance)
        return None



######################################################################
######################################################################
if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("plastique") # http://stackoverflow.com/questions/14200167/how-to-choose-the-graphic-toolkit-for-pyqt
    mw = MainWindow()
    mw.show()
    screen = QApplication.desktop().screenGeometry()
    mw.move( screen.center() - mw.rect().center() )
    sys.exit(app.exec_())
