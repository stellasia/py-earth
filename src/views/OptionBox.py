#!/usr/bin/python
# -*- coding: utf-8 -*-

""" 
MainWindow containing a 
ControlPannel and an 
ImageDisplay widget. 
Also defines the menu.
"""
#author: E. Scifo
#date: 28/12/13
#licence: 

######################################################################
import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import * 


######################################################################
class OptionBox(QDialog):

    ##================================
    def __init__(self):
        """ """ 
        super(OptionBox, self).__init__() # The super() method returns the parent object of the Example class and we call its constructor

        self.setWindowTitle(self.tr("Options"))
        self.InitUI()
        

    ##================================
    def InitUI(self):
        """ GUI interface. """

        layout_config=QHBoxLayout()  # layout for the central widget

        layout_lan=QVBoxLayout()  # layout for the language widget        
        self._lan_group=QButtonGroup(self) # Lan group
        r0=QRadioButton(u"fr")
        r0.setChecked(True)
        self._lan_group.addButton(r0)
        r1=QRadioButton("en")
        self._lan_group.addButton(r1)
        r2=QRadioButton("it")
        self._lan_group.addButton(r2)
        layout_lan.addWidget(r0)
        layout_lan.addWidget(r1)
        layout_lan.addWidget(r2)
        groupbox_lan = QGroupBox(self.tr("Language"))
        groupbox_lan.setLayout(layout_lan)
        

        layout_sty=QVBoxLayout()  # layout for the style widget        
        self._sty_group=QButtonGroup(self) # Sty group
        ra=QRadioButton("plastique")
        ra.setChecked(True)
        self._sty_group.addButton(ra)
        rb=QRadioButton("dark_orange")
        self._sty_group.addButton(rb)
        rd=QRadioButton("dark_grey")
        self._sty_group.addButton(rd)
        # rc=QRadioButton("notebook")
        # self._sty_group.addButton(rc)
        layout_sty.addWidget(ra)
        layout_sty.addWidget(rb)
        layout_sty.addWidget(rd)
        # layout_sty.addWidget(rc)
        groupbox_sty = QGroupBox(self.tr("Style"))
        groupbox_sty.setLayout(layout_sty)

        layout_config.addWidget(groupbox_lan)
        layout_config.addWidget(groupbox_sty)

        ## use the QDialogBoxButton for the usual 'Apply' and 'Cancel' buttons
        buttonBox = QDialogButtonBox()
        buttonBox.setStandardButtons(QDialogButtonBox.Save
                                | QDialogButtonBox.Cancel)


        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        layout = QVBoxLayout()
        layout.addLayout(layout_config)
        layout.addWidget(buttonBox)
        self.setLayout(layout)
          

    def getValues(self):
        """ Get the selected values of the different options. """
        return self._lan_group.checkedButton().text(), self._sty_group.checkedButton().text()


    def isModified(self):
        """ Check wether an option was modified or not. """
        return True # temporary...


######################################################################
######################################################################
if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("plastique") # http://stackoverflow.com/questions/14200167/how-to-choose-the-graphic-toolkit-for-pyqt
    dialog = OptionBox()
#    dialog.accepted.connect(QApplication.aboutQt)
    
    if  dialog.exec_()==QDialog.Accepted:
        if dialog.isModified:
            reply = QMessageBox.warning(dialog, 'Warning',
                                         "The changes will only be taken into account once the program is restarted.", QMessageBox.Ok)

            new_options = dialog.getValues()        
            print (new_options)
        # else do nothing, no changes to be taken into account
