#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import random
from PyQt4.QtCore import *
from PyQt4.QtGui import * 


###################################
class ControlPannel(QWidget):
    """ Control pannel for the PyEarth application """

    updateControlPannelSignal = pyqtSignal() # create a signal emited each time the one of the field in the pannel is updated

    ##================================
    def __init__(self, cities, parent=None):         
        """ Initialize the different elements """ 
        QWidget.__init__(self, parent)
        self._parent = parent

#        self._database = DataBase()
        self._cities = cities #self._database.getCities()

        self._mode = 0 # defaulf mode is prudent
        self._start = ""
        self._end = ""
        modes = [self.tr("Prudent"), self.tr("Chance"), self.tr("Adventure")]

        self._main_layout = QVBoxLayout()

        self.InitStartGroup()        
        self.InitModeGroup(modes)       
        self.InitEndGroup()
        self.InitDistGroup()

        self.setLayout(self._main_layout)

        self.updateControlPannelSignal.emit() # signal to init the image with the default values as well   


    ##================================
    def InitStartGroup(self):
        """ This is the first choice for the departure city among the list from the database """ 
        self._startingPoint = QComboBox()
        for i in range(len(self._cities)):
            self._startingPoint.insertItem(i, self._cities[i])        
        self._gb_start = QGroupBox(self.tr("Start"))        
        #u"Départ") 
        l = QHBoxLayout()
        l.addWidget(self._startingPoint)
        self._gb_start.setLayout(l)
        self._main_layout.addWidget(self._gb_start)
        self._startingPoint.activated.connect(self.ComputeDistance)
        self._startingPoint.setCurrentIndex(self._cities.index("Orsay"))
        return None


    ##================================
    def InitModeGroup(self, modes):
        """ Choice of the mode in which the program will be used """ 
        self._radios = QButtonGroup()
        buttons = []
        l = QHBoxLayout()
        QToolTip.setFont(QFont('SansSerif', 12))
        for i in range(len(modes)):
            buttons.append(QRadioButton(modes[i]))
            self._radios.addButton(buttons[i], i) # add button with id i
            l.addWidget(buttons[i])
            if i == 0:
                buttons[i].setChecked(True)
            buttons[i].setToolTip("Choisissez ce mode si") # not visible because of the plastique theme
        self._radios.buttonClicked[int].connect(self.SelectMode)
        self._gb_mode = QGroupBox(self.tr("Mode"))
        self._gb_mode.setLayout(l)
        self._main_layout.addWidget(self._gb_mode)
        return None
        

    ##================================
    def InitEndGroup(self):
        """ This is the arrival city selection, which depends on the mode the program is used """ 
        self._endPointCombo = QComboBox()
        for i in range(len(self._cities)):
            self._endPointCombo.insertItem(i, self._cities[i])        

        self._endPointCombo.activated.connect(self.ComputeDistance)
        self._endPointCombo.setCurrentIndex(self._cities.index("New-York"))

        self._endPointEdit = QLineEdit("")
        self._endPointEdit.setReadOnly(True)

        self._gb_end = QGroupBox(self.tr("End"))
        #u"Arrivée")
        l = QHBoxLayout()
        l.addWidget(self._endPointCombo)
#        self.endPoint.hide()
        l.addWidget(self._endPointEdit)
        self._endPointEdit.hide()

        self._gb_end.setLayout(l)
        self._main_layout.addWidget(self._gb_end)
        return None


    ##================================
    def InitDistGroup(self):
        self._distEdit = QLineEdit("")
        self._distEdit.setReadOnly(True)
        l = QHBoxLayout()
        l.addWidget(self._distEdit)
        self._gb_dist = QGroupBox(self.tr("Distance"))
        self._gb_dist.setLayout(l)
        self._main_layout.addWidget(self._gb_dist)
        return None


    ##================================
    def SelectMode(self):
        answer = self._radios.checkedId()
#        print answer
        if answer > 0:
            self._endPointCombo.hide()
            self._endPointEdit.show()
        else:
            self._endPointCombo.show()
            self._endPointEdit.hide()
        self._mode=answer # get mode = 0, 1 or 2
#        print self.mode
#        self.update()
        self.ComputeDistance()
        return None


    ##================================
    def SetArrival(self, city):
        """ Change the arrival city display """
        if self._mode>0: # not the prudent mode
            self._endPointEdit.setText(city)
#        self.ComputeDistance()
        return None

    ##================================
    def SetDistance(self, dist):
        text = '{:2.2f} {}'.format(dist, "km") 
        self._distEdit.setText(text)
        return None

    ##================================
    def GetDeparture(self):
        return self._startingPoint.currentText()

    ##================================
    def GetEnd(self):        
        if self._mode>0:
#            print "ControlPannl.GetEnd-->",unicode(self._endPointEdit.currentText())
            return self._endPointEdit.text()
        else:
#            print "ControlPannl.GetEnd-->",unicode(self._endPointCombo.currentText())    
            return self._endPointCombo.currentText()

    ##================================
    def GetMode():
        return self._radios.checkedId()

    ##================================
    def ComputeDistance(self):
        self._start = self._startingPoint.currentText()
        if self._mode == 0:
            self._end = self._endPointCombo.currentText()
#            print "mode prudent"
        elif self._mode == 1:
            #            print "mode joueur"
            self._end = random.choice(self._cities)
            self.SetArrival(self._end)            
        elif self._mode == 2:
            print ("mode aventurier")
            
#        print self.start, self.end
        # dist = 0 #self._database.computeDistance(self._start, self._end)
        # self.SetDistance(dist)
        self.updateControlPannelSignal.emit() # not at the begining in order that the end city exists whatever is the mode.
        return False


###################################
if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("plastique") # http://stackoverflow.com/questions/14200167/how-to-choose-the-graphic-toolkit-for-pyqt
    cp = ControlPannel()
    cp.show()
    sys.exit(app.exec_())
