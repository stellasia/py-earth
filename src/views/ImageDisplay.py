#!/usr/bin/python
# -*- coding: utf-8 -*-

""" 
ImageDisplay and related classes to handle the Earth display and rotation, including the positionning of the start and end coordinates with flags. 

Help from : http://www.richelbilderbeek.nl/CppQtExample1.htm
"""

import sys
import os
from math import cos, sin, pi

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from models.Coordinate import *


######################################################################
## Configuration
FILE_PATH = "images/TheEarth.gif"
NB_FRAMES = 24	
LONG_STEP = 360./NB_FRAMES # step of longitude between two frames
FRAME_ZERO = 12 # frame whose left border corresponds to longitude=0 --> approximately, need a tune parameter to get the exact value...


######################################################################
class Background(QGraphicsPixmapItem):
    """ Test class to display a background image on a graphicsscene. """
    ##================================
    def __init__(self, filename):
        """ """
        super(Background, self).__init__()
        pix = QPixmap(filename);
        self.setPixmap(pix)

    
######################################################################
class ChangingBackground(QGraphicsPixmapItem):
    """ ChangingBackground class to handle the Earth rotation/ The baseline image is the gif image whose path is stored in the global variable FILE_PATH. The base member of this class is a QMovie object to move from frame to frame in the gif image."""

    ##================================
    def __init__(self):
        """ Create the movie and show first frame (frame number 0). """
        super(ChangingBackground, self).__init__()
        fullFilePath = os.path.join(os.path.dirname(__file__), '../../', FILE_PATH)
        self._movie = QMovie(fullFilePath, QByteArray())
        self._movie.setCacheMode(QMovie.CacheAll)
        self._movie.setSpeed(50)
        self.advance()

    ##================================
    def advance(self, direction = 1): # go to next frame
        """ Advance by one frame. If directioin < 0, move backward."""
        current_frame = self._movie.currentFrameNumber()
        number_of_frames = self._movie.frameCount()
        if direction > 0: # move forward
            next_frame = current_frame+1
            if next_frame > number_of_frames:
                next_frame = 0
        else: # move backward
            next_frame = current_frame-1
            if next_frame < 0:
                next_frame = number_of_frames-1
        self._movie.jumpToFrame(next_frame)
        pix = self._movie.currentPixmap()
        self.setPixmap(pix)
        self._current_frame_number = self._movie.currentFrameNumber()


    ##================================
    def getLongitudeRef(self):
        """ Get the longitude of the left border of the current frame. """
        long_ref = -(self._current_frame_number-FRAME_ZERO)*LONG_STEP+5 # '+5' is an arbitrary parameter to tune the localisation on the map
        return long_ref


######################################################################
class TransparentSprite(QGraphicsPixmapItem):
    """ Class TransparentSprite handle the flag of the start and end point on the map. """

    ##================================
    def __init__(self, filename, coo=[], transparent_color = QColor(255, 255, 255)):
        """ Instantiaate an instance of TransparentSprite usinng the image filename as baseline and a transparent color. 'coo' is a Coordinates object used to draw the flag at the right place on the map. """
        super(TransparentSprite, self).__init__()
        self._dx = 10
        self._dy = 10
        self._maxx = 320
        self._maxy = 200        
        pix = QPixmap(filename);
        mask = pix.createMaskFromColor(transparent_color)
        pix.setMask(mask) 
        self.setPixmap(pix)
        if len(coo)>0:
            self.setCoo(coo)

    ##================================
    def move(self, ref_long):
        """ Move the sprite when the earth is rotating. 'ref_long' refers to the longitude of the left border of the current background frame. """
        RAYON_IMAGE = 525/2. # pixels
        ref_long_rad = pi*ref_long/180.
        angle_azimuthal = self._coo.longitude_rad - ref_long_rad - pi/2.
        angle_polar =  self._coo.latitude_rad
        newz = cos(angle_polar)*cos(angle_azimuthal) 
        if newz<0: # if z<0, point is not visible
            self.hide()
            return None
        else:
            self.show()
        newx = cos(angle_polar)*sin(angle_azimuthal)*RAYON_IMAGE
        newy = sin(angle_polar)*RAYON_IMAGE
        newx = RAYON_IMAGE+newx
        newy = RAYON_IMAGE-newy
        newx = newx-self.pixmap().size().width()*0.2/2
        newy = newy-self.pixmap().size().height()*0.2
        self.setX(newx)
        self.setY(newy)

    ##================================
    def advance(self, direction=1):
        """ Old method, do not use anymmore"""
        if direction>0:
            newx = self.x()+self._dx
            newy = self.y()+self._dy
        else:
            newx = self.x()-self._dx
            newy = self.y()-self._dy            
        if newx<0 or newx>self._maxx:
            self._dx= -self._dx
        if newy<0 or newy>self._maxy:
            self._dy= -self._dy
        self.setX(newx)
        self.setY(newy)

    ##================================
    def setRect(self, w, h):
        """ """
        self._maxx = w - self.pixmap().width()
        self._maxy = h - self.pixmap().height()


    ##================================
    def setCoo(self, coo):
        """ Set the coordinates """
        # still need to check 'coo' has a valid format
        self._coo = coo
        return None



######################################################################
class View(QGraphicsView):
    """ View all the elements: the background and the two flags. """
    ##================================
    def __init__(self):
        """ """
        super(View, self).__init__()
        self._sprite_start = TransparentSprite(os.path.join(os.path.dirname(__file__), '../../', "images/drap_start.png"))
        self._sprite_start.scale(0.2, 0.2)

        self._sprite_end = TransparentSprite(os.path.join(os.path.dirname(__file__), '../../', "images/drap_end.png"))
        self._sprite_end.scale(0.2, 0.2)

        self._background = ChangingBackground()
        self._scene = QGraphicsScene()
        self._scene.addItem(self._background)
        self._scene.addItem(self._sprite_start)
        self._scene.addItem(self._sprite_end)
        self.setScene(self._scene)
        self.setStyleSheet("background-color: #000019");  
        self._long_ref = self._background.getLongitudeRef()
        return None


    ##================================
    def advance(self, direction=1):
        """ Advance the scene ie advance the background and move the start and end sprites accordingly. """        
        self._background.advance(direction)
        self._long_ref = self._background.getLongitudeRef()
        self._sprite_start.move(self._long_ref)
        self._sprite_end.move(self._long_ref)
        return None


    ##================================
    def updateCoo(self, coo_start, coo_end):
        """ Slot used when the control pannel is updated. """
        self._sprite_start.setCoo(coo_start)
        self._sprite_end.setCoo(coo_end)
        self.advance()
        return None



######################################################################
class ImageDisplay(QWidget):
    """ Scene plus buttons """

    ##================================
    def __init__(self):
        """ """
        super(ImageDisplay, self).__init__()

        self._InitButtons()

        self._view = View()
        self._view.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum) # force the view to adopt the background size

        self._view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff) # do not allow scrollbars (allow to put hiden item out of the displayed screen)
        self._view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self._main_layout = QVBoxLayout()
        self._main_layout.addWidget(self._view)
        self._main_layout.addLayout(self._button_layout)
        self.setLayout(self._main_layout)


    ##================================
    def _InitButtons(self):
        """ """
        self._button_layout = QHBoxLayout()

        self._play_pause_btn = QToolButton( self)
        self._play_pause_btn.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self._play_pause_btn.setIcon(QIcon(os.path.join(os.path.dirname(__file__), '../../', 'images/play.png')))
        self._play_pause_btn.setIconSize(QSize(50,50))
        self._play_pause_btn.setText(self.tr('Start/Pause'))
        self._button_layout.addWidget(self._play_pause_btn)

        self._previous_btn = QToolButton(self)
        self._previous_btn.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self._previous_btn.setIcon(QIcon(os.path.join(os.path.dirname(__file__), '../../', 'images/previous.png')))
        self._previous_btn.setIconSize(QSize(50,50))
        self._previous_btn.setText(self.tr('Previous Frame'))
        self._button_layout.addWidget(self._previous_btn)
        self.connect(self._previous_btn, SIGNAL("clicked()"), self.previous)

        self._next_btn = QToolButton(self)
        self._next_btn.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self._next_btn.setIcon(QIcon(os.path.join(os.path.dirname(__file__), '../../', 'images/next.png')))
        self._next_btn.setIconSize(QSize(50,50))
        self._next_btn.setText(self.tr('Next Frame'))
        self._button_layout.addWidget(self._next_btn)
        self.connect(self._next_btn, SIGNAL("clicked()"), self.next)

        return None


    ##================================
    def next(self):
        """ Go to next frame """
        self._view.advance(1) #_background.advance(1)
        return None


    ##================================
    def previous(self):
        """ Go to previous frame. """
        self._view.advance(-1)
        return None


    ##================================
    def mousePressEvent(self, e):
        """ No used yet, get the coordinates of the pointer when a mouse button is pressed. """
        super(ImageDisplay, self).mousePressEvent(e)
        position = QPointF(e.pos())
#        print position.x() , position.y()
    

    ##================================
    def update(self, coo_start, coo_end):
        """ Control pannel has been changed, need to update the view. """
        self._view.updateCoo(coo_start, coo_end)
        return None



######################################################################
######################################################################
if __name__=='__main__':
    app = QApplication(sys.argv)
    main = ImageDisplay()
    main.setWindowTitle('ImageDisplay')
    main.show()
    screen = QApplication.desktop().screenGeometry()
    main.move( screen.center() - main.rect().center() )
    sys.exit(app.exec_())
