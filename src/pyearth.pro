SOURCES          += pyearth.py
SOURCES          += UI_OptionBox.py
SOURCES          += UI_ImageDisplay.py
SOURCES          += UI_ControlPannel.py
SOURCES          += UI_MainWindow.py
TRANSLATIONS     += pyearth_fr.ts
TRANSLATIONS     += pyearth_en.ts
TRANSLATIONS     += pyearth_it.ts
