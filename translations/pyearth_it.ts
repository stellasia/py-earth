<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="it" sourcelanguage="">
<context>
    <name>ControlPannel</name>
    <message>
        <location filename="UI_ControlPannel.py" line="48"/>
        <source>Start</source>
        <translation>Partenza</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="74"/>
        <source>Mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="92"/>
        <source>End</source>
        <translation>Arrivo</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="111"/>
        <source>Distance</source>
        <translation>Distanza</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="30"/>
        <source>Prudent</source>
        <translation>Attento</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="30"/>
        <source>Chance</source>
        <translation>Giocatore</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="30"/>
        <source>Adventure</source>
        <translation>Avventuriero</translation>
    </message>
</context>
<context>
    <name>ImageDisplay</name>
    <message>
        <location filename="UI_ImageDisplay.py" line="239"/>
        <source>Start/Pause</source>
        <translation>Anima/Pausa</translation>
    </message>
    <message>
        <location filename="UI_ImageDisplay.py" line="246"/>
        <source>Previous Frame</source>
        <translation>Precedente</translation>
    </message>
    <message>
        <location filename="UI_ImageDisplay.py" line="254"/>
        <source>Next Frame</source>
        <translation>Prossima</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="UI_MainWindow.py" line="88"/>
        <source>Ready</source>
        <translation>Pronto</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="115"/>
        <source>Exit application</source>
        <translation>Uscire</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="120"/>
        <source>French DB</source>
        <translation>Banca-dati francese</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="125"/>
        <source>World DB</source>
        <translation>Banca-dati mondo</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="129"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="132"/>
        <source>&amp;Change database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="111"/>
        <source>&amp;Language</source>
        <translation type="obsolete">&amp;Lingua</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="142"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="145"/>
        <source>About</source>
        <translation>A proposito</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="113"/>
        <source>&amp;Exit</source>
        <translation>&amp;Uscire</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="118"/>
        <source>Use French (&amp;Default) DB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="123"/>
        <source>Use &amp;Wolrd DB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="131"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="113"/>
        <source>Language</source>
        <translation type="obsolete">Lingua</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="143"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="136"/>
        <source>&amp;Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="138"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="171"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="171"/>
        <source>The changes will only be taken into account once the program is restarted.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionBox</name>
    <message>
        <location filename="UI_OptionBox.py" line="28"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="UI_OptionBox.py" line="50"/>
        <source>Language</source>
        <translation type="unfinished">Lingua</translation>
    </message>
    <message>
        <location filename="UI_OptionBox.py" line="63"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
