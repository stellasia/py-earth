<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ControlPannel</name>
    <message>
        <location filename="UI_ControlPannel.py" line="48"/>
        <source>Start</source>
        <translation>Départ</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="74"/>
        <source>Mode</source>
        <translation>Mode</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="92"/>
        <source>End</source>
        <translation>Arrivée</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="111"/>
        <source>Distance</source>
        <translation>Distance</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="30"/>
        <source>Prudent</source>
        <translation>Prudent</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="30"/>
        <source>Chance</source>
        <translation>Joueur</translation>
    </message>
    <message>
        <location filename="UI_ControlPannel.py" line="30"/>
        <source>Adventure</source>
        <translation>Aventurier</translation>
    </message>
</context>
<context>
    <name>ImageDisplay</name>
    <message>
        <location filename="UI_ImageDisplay.py" line="239"/>
        <source>Start/Pause</source>
        <translation>Jouer/Pause</translation>
    </message>
    <message>
        <location filename="UI_ImageDisplay.py" line="246"/>
        <source>Previous Frame</source>
        <translation>Image précédente</translation>
    </message>
    <message>
        <location filename="UI_ImageDisplay.py" line="254"/>
        <source>Next Frame</source>
        <translation>Image suivante</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="UI_MainWindow.py" line="88"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="115"/>
        <source>Exit application</source>
        <translation>Sortir du programme</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="120"/>
        <source>French DB</source>
        <translation>Bdd &apos;France&apos;</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="125"/>
        <source>World DB</source>
        <translation>Bdd &apos;Monde&apos;</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="129"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="132"/>
        <source>&amp;Change database</source>
        <translation>&amp;Changer de base de données</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="111"/>
        <source>&amp;Language</source>
        <translation type="obsolete">&amp;Langue</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="142"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="145"/>
        <source>About</source>
        <translation>A-propos</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="113"/>
        <source>&amp;Exit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="118"/>
        <source>Use French (&amp;Default) DB</source>
        <translation>Bdd &apos;France&apos; (&amp;Defaul)</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="123"/>
        <source>Use &amp;Wolrd DB</source>
        <translation>Bdd &apos;&amp;Monde&apos;</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="131"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="113"/>
        <source>Language</source>
        <translation type="obsolete">Langue</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="143"/>
        <source>&amp;About</source>
        <translation>A &amp;Propos</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="136"/>
        <source>&amp;Options</source>
        <translation>&amp;Options</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="138"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="171"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="UI_MainWindow.py" line="171"/>
        <source>The changes will only be taken into account once the program is restarted.</source>
        <translation>Les changements ne seront pris en compte que lors du prochain redémarrage du programme.</translation>
    </message>
</context>
<context>
    <name>OptionBox</name>
    <message>
        <location filename="UI_OptionBox.py" line="28"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="UI_OptionBox.py" line="50"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="UI_OptionBox.py" line="63"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
</context>
</TS>
